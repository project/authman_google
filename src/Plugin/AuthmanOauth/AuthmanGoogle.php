<?php

declare(strict_types = 1);

namespace Drupal\authman_google\Plugin\AuthmanOauth;

use Drupal\authman\AuthmanInstance\AuthmanOauthInstanceInterface;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginBase;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginResourceOwnerInterface;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginScopeInterface;
use Drupal\authman\Plugin\KeyType\OauthClientKeyType;
use Drupal\authman_google\AuthmanInstance\AuthmanOauthGoogleInstance;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\key\KeyInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Google OAuth provider.
 *
 * @AuthmanOauth(
 *   id = "authman_google",
 *   label = @Translation("Google"),
 *   grant_types = {
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_AUTHORIZATION_CODE,
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_CLIENT_CREDENTIALS,
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_REFRESH_TOKEN,
 *   },
 * )
 *
 * @internal
 */
class AuthmanGoogle extends AuthmanOauthPluginBase implements ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface, AuthmanOauthPluginResourceOwnerInterface, AuthmanOauthPluginScopeInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('authman_google.http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'offline' => TRUE,
      'scopes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance(array $providerOptions, string $grantType, KeyInterface $clientKey): AuthmanOauthInstanceInterface {
    $keyType = $clientKey->getKeyType();
    assert($keyType instanceof OauthClientKeyType);
    $provider = $this->createProvider($providerOptions, $clientKey);
    return (new AuthmanOauthGoogleInstance($provider, $grantType))
      ->setScopes($this->getConfiguration()['scopes']);
  }

  /**
   * {@inheritdoc}
   */
  protected function createProvider(array $providerOptions, $clientKey): AbstractProvider {
    if (!empty($this->getConfiguration()['offline'])) {
      // Refresh tokens are only provided to applications which request offline
      // access. We need a refresh token to be able to automate token creation,
      // since we dont want the user to have to login every hour to fix auth.
      $providerOptions['accessType'] = 'offline';
    }

    $values = $clientKey->getKeyValues();
    $provider = new Google([
      'clientId' => $values['client_id'] ?? '',
      'clientSecret' => $values['client_secret'] ?? [],
    ] + $providerOptions);
    $provider->setHttpClient($this->httpClient);
    return $provider;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['offline'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Offline access'),
      '#default_value' => $this->getConfiguration()['offline'],
      '#access' => FALSE,
    ];

    $scopes = implode("\r\n", $this->getConfiguration()['scopes']);
    $form['scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Scopes'),
      '#description' => $this->t('Scopes to request access. Account needs to be re-authenticated if these values are changed.'),
      '#default_value' => $scopes,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['offline'] = (bool) $form_state->getValue('offline');
    $scopes = array_filter(explode("\r\n", $form_state->getValue('scopes')));
    $this->configuration['scopes'] = $scopes;
  }

  /**
   * {@inheritdoc}
   */
  public function renderResourceOwner(ResourceOwnerInterface $resourceOwner): array {
    assert($resourceOwner instanceof GoogleUser);
    $build = [];
    $build['owner'] = [
      '#theme' => 'authman_google_resource_owner',
      'name' => $resourceOwner->getName(),
      'email' => $resourceOwner->getEmail(),
      'avatar_url' => $resourceOwner->getAvatar(),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopes(): array {
    return array_values($this->getConfiguration()['scopes'] ?? []);
  }

}
