Authman Google

https://www.drupal.org/project/authman_google

Plugin implementation of Google OAuth 2.0 for [Authman][authman]

[authman]: https://www.drupal.org/project/authman

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Example

Example from YouTube API: https://developers.google.com/youtube/v3/docs/videos/list#common-use-cases

This example requires scope `https://www.googleapis.com/auth/youtube.readonly`.

```php
/** @var \Drupal\authman\AuthmanInstance\AuthmanOauthFactoryInterface $factory */
$factory = \Drupal::service('authman.oauth');
$authmanInstance = $factory->get('foo');
try {
  $response = $authmanInstance->authenticatedRequest('GET', 'https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&myRating=like');
  $successJson = \json_decode((string) $response->getBody());
}
catch (\GuzzleHttp\Exception\GuzzleException $e) {
  $errorJson = \json_decode((string) $e->getResponse()->getBody());
}
```
